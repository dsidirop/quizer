<img src="https://d33wubrfki0l68.cloudfront.net/49c2be6f2607b5c12dd27f8ecc8521723447975d/f05c5/logo-small.cbbeba89.svg" alt="Nest Js logo" title="Nest Js" height="96" width="96" align="right"/> <img src="https://storage.googleapis.com/gweb-cloudblog-publish/images/cloud_function_firebase.max-400x400.png" alt="Firebase Cloud Functions logo" title="Firebase Cloud Functions" height="96" width="96" align="right"/>
# Quizer

[![Status](https://gitlab.com/dsidirop/quizer/badges/master/pipeline.svg)](https://gitlab.com/dsidirop/quizer/-/commits/master)
[![Coverage](https://gitlab.com/dsidirop/quizer/badges/master/coverage.svg)](https://gitlab.com/dsidirop/quizer/-/commits/master)

[SonarQube](https://sonarcloud.io/dashboard?id=dsidirop_quizer):

# [![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=dsidirop_quizer&metric=alert_status)](https://sonarcloud.io/dashboard?id=dsidirop_quizer)
[![SonarCloud Coverage](https://sonarcloud.io/api/project_badges/measure?project=dsidirop_quizer&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=dsidirop_quizer)
[![SonarCloud Bugs](https://sonarcloud.io/api/project_badges/measure?project=dsidirop_quizer&metric=bugs)](https://sonarcloud.io/component_measures/metric/reliability_rating/list?id=dsidirop_quizer)
[![SonarCloud Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=dsidirop_quizer&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/security_rating/list?id=dsidirop_quizer)

## Overview

Firebase / Angular Quiz-App Coupled with an Aggressive CI/CD Pipeline:

1. Login / Signup / Logout logic via FB Google or email/password (using Firebase Authentication tools).

2. Welcome page that shows user’s total score and a button to start a new Quiz. Each quiz consists of 5 questions. Each question consists of 4 answers.

3. The user presses the button to starts the quiz and to respond to all 5 question by selecting one of the 4 answers.\
\
Each question has a countdown time (10 secs by default unless a question alots a different time duration). After said time is up the answer is considered as wrong and the user is redirected to the next question.

4. At the end of the 5 questions the user is shown a thank you page that shows the achieved score for this try (pe 2/5 or 40%), the overall total score across all sessions (pe if the user has played 10 rounds in total this score could be 40/50 which is 80%) as well as a button to play again.

5. If the user presses the button to play again, a new set of 5 questions is generated.

Stack:

- Angular 9.1.3
- Firebase 8.4.1 with Cloud Firestore
- Node Web Service API to get the QA json content

## Deploy

- `cd quizer`
- `npm run firebase-deploy` (keep in mind that it might take a few mins before your build deployment is actually reflected on google's servers!)
- `https://quizer-d6b60.web.app/`

## Development server

`cd quizer` and run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

`cd quizer` and run `npm run test:ci` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

`cd quizer` and run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `cd quizer` and `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Resources

- Building an API with Firebase:

  https://indepth.dev/building-an-api-with-firebase/

- Firebase CLI:

  https://firebase.google.com/docs/cli#windows-npm

- Firebase Facebook Login with Angular:

  https://www.positronx.io/full-angular-7-firebase-authentication-system/
  https://www.positronx.io/firebase-facebook-login-auth-provider-work-with-angular/

- Where can I find the API KEY for Firebase Cloud Messaging?

  https://stackoverflow.com/a/37338923/863651

- Where can I find my Firebase ApiKey and AuthDomain

  https://stackoverflow.com/questions/52500573/where-can-i-find-my-firebase-apikey-and-authdomain

- Create Facebook App for Social Login

  https://www.youtube.com/watch?v=Z9nVdZX9PHw

- How to specialize Angular Layouts for Login and Home Page

  https://loiane.com/2017/08/angular-hide-navbar-login-page/#example-2-using-different-layouts-and-routing-config

- Configuring Website Deployment

  https://youtu.be/aICeVhu2mAE?t=48

- Deploying an Angular CLI App to Production with Firebase

  https://scotch.io/tutorials/deploying-an-angular-cli-app-to-production-with-firebase

- How to enable and configure ESLint in Angular Projects

  https://thesoreon.com/blog/how-to-set-up-eslint-with-typescript-in-vs-code

- How to enable and configure Prettier in a way that works well with ESLint for Angular Projects

  https://medium.com/@victormejia/setting-up-prettier-in-an-angular-cli-project-2f50c3b9a537

- Setting Cloud Firestore Security Rules Primer

  https://medium.com/@khreniak/cloud-firestore-security-rules-basics-fac6b6bea18e

- Firestore CRUD and pagination

  https://alligator.io/angular/cloud-firestore-angularfire/
  https://blog.angular.io/improved-querying-and-offline-data-with-angularfirestore-dab8e20a4a06/
  https://medium.com/@AnkitMaheshwariIn/pagination-in-angular-firestore-firebase-database-add-get-documents-14ca723e9c24/
  https://github.com/firebase/snippets-web/blob/cee9068490fbfb03c123fbda87bc08a73a122520/firestore/test.firestore.js#L948-L948

- Angular FontAwesome Jumpstart

  https://haricodes.com/angular-quickstart-font-awesome

- Firebase Unit Testing

  https://firebase.google.com/docs/functions/unit-testing

- NestJs for Firebase Cloud Functions

  https://github.com/IngAjVillalon/NestJs-Firebase-Cloud-Functions-Initial-Template
