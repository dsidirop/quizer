# Quizer

Firebase / Angular Demo

### Todo

- [ ] Make services return observables instead of promises  
- [ ] Improve Code-Coverage > 80%  
- [ ] FontAwesome  
- [ ] Try setting environment.prod from environment like so: environment = { ...enviromentDev, production: true } etc  
- [ ] 100 Firebase Tips: https://www.youtube.com/watch?v=iWEgpdVSZyg  
- [ ] Replace margin-top with margin-bottom  
- [ ] Integrate Akita  
- [ ] Split Project in Modules  
- [ ] Explore whether we can get rid of setting the currently logged in user to the local-storage  
- [ ] Add support for Anonymous login  
- [ ] Parametrize deployment (master vS rc/beta vS dev/alpha)  

### In Progress

- [ ] CRUD Rest API  

### Done ✓

- [x] Testing in headless mode via https://github.com/angular/angular-cli/issues/2013#issuecomment-343272011  
- [x] Check for errors when sending the verification email  
- [x] IAppLocalStorageService extends IAppStorageService <- reconsider this association - it seems to create more problems than it solves!  
- [x] IAppStorageService.remove should not be part of AppLocalStorageService[Mock]!  
- [x] ⭐ Test  
- [x] Use transducers for transformations to reduce memory overhead  
- [x] Use let/const instead of var  
- [x] Fix tests  
- [x] Add SonarQube and Linting in Gitlab CI  
- [x] [WONTFIX] InjectionToken + ProvideIn root https://angular.io/api/core/InjectionToken  

