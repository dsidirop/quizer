import { FirebaseOptions } from '@angular/fire';
import { BuildInfo } from '@app-shared-contracts/models/build-info.model';
import { IGlobalConfiguration } from '@app-shared-contracts/models/iglobal-configuration.model';
import * as buildInfo from '@root/build-info.json';
import { version } from '@root/package.json';

export const environment: IGlobalConfiguration = {
  production: true,

  version: version,
  buildInfo: buildInfo as BuildInfo,

  localStorageNamespace: 'quizer',

  firebaseConfig: {
    appId: '1:207245018986:web:3b919935649a678cbe4713',
    apiKey: 'AIzaSyA2ZreEURxa93qQS6N23N9nemDtAXGw1zk',
    projectId: 'quizer-d6b60',
    authDomain: 'quizer-d6b60.firebaseapp.com',
    databaseURL: 'https://quizer-d6b60.firebaseio.com',
    measurementId: 'G-4BL5MZGMV6',
    storageBucket: 'quizer-d6b60.appspot.com',
    messagingSenderId: '207245018986'
  } as FirebaseOptions
};
