import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '@app-env/environment';
import { IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { IUtilsServiceToken } from '@app-shared-contracts/helpers/iutils.service';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { IAppGenericStorageServiceFactoryToken } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-service.factory';
import { IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { IMapperServiceToken } from '@app-shared-contracts/services/mapper/imapper.service';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';
import { GlobalConfigurationService } from '@app-shared/configuration/global-configuration.service';
import { MapperService } from '@app-shared/mapper/mapper.service';
import { AppRoutingModule } from '@app-shared/routing/app-routing.module';
import { AuthService } from '@app-shared/services/firestore/auth.service';
import { QuestionsService } from '@app-shared/services/firestore/questions.service';
import { UsersService } from '@app-shared/services/firestore/users.service';
import { UtilsService } from '@app-shared/services/helpers/utils.service';
import { AppGenericStorageServiceFactory } from '@app-shared/services/local-storage/app-generic-storage-service.factory';
import { AppLocalStorageService } from '@app-shared/services/local-storage/app-local-storage.service';
import { NavigationService } from '@app-shared/services/navigation/navigation.service';
import { NewQuizComponent } from '@app/components/new-quiz/new-quiz.component';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { DefaultLayoutComponent } from './components/layouts/default-layout/default-layout.component';
import { SignInLayoutComponent } from './components/layouts/sign-in-layout/sing-in-layout.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { IQuestionsServiceToken } from './shared-contracts/services/firestore/iquestions.service';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    NewQuizComponent,
    DashboardComponent,
    AppBaselineComponent,
    VerifyEmailComponent,
    SignInLayoutComponent,
    DefaultLayoutComponent,
    ForgotPasswordComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    ReactiveFormsModule,
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [
    { provide: IAuthServiceToken, useClass: AuthService },
    { provide: IUtilsServiceToken, useClass: UtilsService },
    { provide: IUsersServiceToken, useClass: UsersService },
    { provide: IMapperServiceToken, useClass: MapperService },
    { provide: IQuestionsServiceToken, useClass: QuestionsService },
    { provide: INavigationServiceToken, useClass: NavigationService },
    { provide: IAppLocalStorageServiceToken, useClass: AppLocalStorageService },
    { provide: IGlobalConfigurationServiceToken, useClass: GlobalConfigurationService },
    { provide: IAppGenericStorageServiceFactoryToken, useClass: AppGenericStorageServiceFactory }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
