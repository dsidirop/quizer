import { Component, Inject, OnInit } from '@angular/core';
import { UserData } from '@app-shared-contracts/models/user-data.model';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { IUsersService, IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';
import { Observable } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends AppBaselineComponent implements OnInit {

  public userData$: Observable<UserData>;
  public userTidbits$: Observable<UserTidbits>;

  public constructor(
    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(IUsersServiceToken) private readonly srvUsers: IUsersService
  ) { super(); }

  public ngOnInit(): void {
    this.userData$ = this
      .srvAuth
      .authStateListener()
      .pipe(takeUntil(this.ngUnsubscribe$));

    this.userTidbits$ = this
      .userData$
      .pipe(
        map(userData => userData?.uid),
        filter(uid => !!uid), //ensure logged in
        switchMap(uid => this
          .srvUsers
          .usersTidbitsChangesListener(uid)
          .pipe(
            takeUntil(this.ngUnsubscribe$)
          ))
      );
  }

}
