import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { UsersServiceMock } from '@app/testing/mocks/users.service.mock';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: IUsersServiceToken, useClass: UsersServiceMock }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
