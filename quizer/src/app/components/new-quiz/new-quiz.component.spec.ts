import { IUtilsServiceToken } from './../../shared-contracts/helpers/iutils.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { IQuestionsServiceToken } from '@app-shared-contracts/services/firestore/iquestions.service';
import { IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { GlobalConfigurationService } from '@app-shared/configuration/global-configuration.service';
import { AppLocalStorageServiceMock } from '@app/testing/mocks/app-local-storage.service.mock';
import { QuestionsServiceMock } from '@app/testing/mocks/questions.service.mock';
import { UsersServiceMock } from '@app/testing/mocks/users.service.mock';
import { NewQuizComponent } from './new-quiz.component';
import { UtilsService } from '@app-shared/services/helpers/utils.service';

describe('NewQuizComponent', () => {
  let component: NewQuizComponent;
  let fixture: ComponentFixture<NewQuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewQuizComponent],
      providers: [
        { provide: IUsersServiceToken, useClass: UsersServiceMock },
        { provide: IQuestionsServiceToken, useClass: QuestionsServiceMock },
        { provide: IAppLocalStorageServiceToken, useClass: AppLocalStorageServiceMock },

        { provide: IUtilsServiceToken, useClass: UtilsService },
        { provide: IGlobalConfigurationServiceToken, useClass: GlobalConfigurationService }
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
