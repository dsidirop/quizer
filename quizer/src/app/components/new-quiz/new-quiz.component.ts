import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IGlobalConfigurationService, IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { Question } from '@app-shared-contracts/models';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';
import { IQuestionsService, IQuestionsServiceToken } from '@app-shared-contracts/services/firestore/iquestions.service';
import { IUsersService, IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { IAppLocalStorageService, IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';
import { isUndefined as _isUndefined } from 'lodash';
import { Observable, Subject, timer } from 'rxjs';
import { scan, takeUntil, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-new-quiz',
  templateUrl: './new-quiz.component.html',
  styleUrls: ['./new-quiz.component.scss']
})
export class NewQuizComponent extends AppBaselineComponent implements OnInit {

  public errors = { occurred: false };
  public questions: Question[];

  public timeRemaining: number;
  public quizCompleted = false;
  public currentQuestion: Question;
  public multipleAnswersForm: FormGroup;

  public currentQuestionIndex: number;
  public questionsCurrentBatchSize: number;

  public userTidbits: UserTidbits;
  public correctAnswersCount = 0;

  private uid: string;
  private requestToMoveToNextQuestion$: Subject<void>;

  public constructor(
    @Inject(IUsersServiceToken) private readonly srvUsers: IUsersService,
    @Inject(IQuestionsServiceToken) private readonly srvQuestions: IQuestionsService,
    @Inject(IAppLocalStorageServiceToken) private readonly srvLocalStorage: IAppLocalStorageService,
    @Inject(IGlobalConfigurationServiceToken) private readonly srvGlobalConfiguration: IGlobalConfigurationService
  ) {
    super();
  }

  public ngOnInit(): void {
    this.uid = this.srvLocalStorage.getUserData().uid;
    this.requestToMoveToNextQuestion$ = new Subject<void>();

    this
      .requestToMoveToNextQuestion$
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(this.requestToMoveToNextQuestion$_next);

    this.kickstartAsync(this.uid); //cant use await here but its ok
  }

  public btnTryAgain_clicked = async (): Promise<void> => {
    await this.kickstartAsync(this.uid);
  };

  private kickstartAsync = async (uid: string) => {
    this.errors.occurred = false;

    this.questions = [];
    this.quizCompleted = false;
    this.correctAnswersCount = 0;
    this.currentQuestionIndex = -1;
    this.questionsCurrentBatchSize = 0;

    try {
      this.userTidbits = await this
        .srvUsers
        .getUsersTidbits(uid)
        .pipe(
          takeUntil(this.ngUnsubscribe$)
        )
        .toPromise();

      this.questions = await this
        .srvQuestions
        .getQuestions(
          this.userTidbits.questionsPageIndex,
          this.srvGlobalConfiguration.questionsCurrentBatchSize
        )
        .toPromise();

      this.questionsCurrentBatchSize = this.questions.length;
    } catch (err) {
      console.error(err);
      this.errors.occurred = true;
      return;
    }

    if (this.questions.length === 0) { //no more questions left so we reset back to the first page
      this.userTidbits.questionsPageIndex = 0; //order
      await this.srvUsers.updateUserTidbitsAsync({ uid: this.uid, questionsPageIndex: 0 }); //order
      await this.kickstartAsync(this.uid); //order
      return;
    }

    this.requestToMoveToNextQuestion$.next(); //kickstart
  }

  private requestToMoveToNextQuestion$_next = async () => {
    if (this.currentQuestionIndex >= this.questionsCurrentBatchSize - 1) {
      this.userTidbits.totalNumberOfQuestions += this.questionsCurrentBatchSize; //order
      this.userTidbits.numberOfQuestionsAnsweredCorrectly += this.correctAnswersCount; //order
      await this.srvUsers.updateUserTidbitsAsync({ //order
        uid: this.uid,
        questionsPageIndex: this.userTidbits.questionsPageIndex + 1,
        totalNumberOfQuestions: this.userTidbits.totalNumberOfQuestions,
        numberOfQuestionsAnsweredCorrectly: this.userTidbits.numberOfQuestionsAnsweredCorrectly
      });

      this.questions = [];
      this.quizCompleted = true;
      return;
    }

    this.currentQuestion = this.questions[++this.currentQuestionIndex]; //order
    this
      .respawnTimer(this.currentQuestion.timeToAnswer) //order
      .subscribe(timeRemaining => {
        this.timeRemaining = timeRemaining;
        if (this.timeRemaining === 0) {
          this.requestToMoveToNextQuestion$.next(); //1
        }
      });

    //1 if the user forfeits on time then we signal the behavioursubject to move to the next question automatically
  };

  public radioButtonAnyPossibleAnswer_changed = (questionId: string, suppliedAnswerId: number): void => {
    const correctAnswer = this.questions.filter(x => x.id === questionId)[0];
    if (_isUndefined(correctAnswer))
      throw new Error('[NQ.RBAPAC01] [BUG] Failed to find answer! (how did this happen?)');

    this.correctAnswersCount += suppliedAnswerId === correctAnswer.correctAnswer ? 1 : 0;
    this.requestToMoveToNextQuestion$.next();
  }

  private respawnTimer = (timeToAnswer?: number): Observable<number> => {
    this.timeRemaining = timeToAnswer ?? this.srvGlobalConfiguration.defaultTimeToAnswer;
    return timer(0, NewQuizComponent.OneSecondInMillisecs).pipe(
      scan(acc => acc - 1, this.timeRemaining + 1),
      takeUntil(this.ngUnsubscribe$),
      takeUntil(this.requestToMoveToNextQuestion$) //0 play it safe
    );

    //0 when the system moves from one question to ensure that the timer gets destroyed and recreated
  }

  private static readonly OneSecondInMillisecs = 1000;
}
