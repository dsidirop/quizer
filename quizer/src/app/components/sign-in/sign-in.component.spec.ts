import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration';
import { IUtilsServiceToken } from '@app-shared-contracts/helpers/iutils.service';
import { IAuthServiceToken } from '@app-shared-contracts/services';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { GlobalConfigurationService } from '@app-shared/configuration';
import { UtilsService } from '@app-shared/services/helpers/utils.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { NavigationServiceMock } from '@app/testing/mocks/navigation.service.mock';
import { SignInComponent } from './sign-in.component';

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        SignInComponent
      ],
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: INavigationServiceToken, useClass: NavigationServiceMock },

        { provide: IUtilsServiceToken, useClass: UtilsService },
        { provide: IGlobalConfigurationServiceToken, useClass: GlobalConfigurationService }
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
