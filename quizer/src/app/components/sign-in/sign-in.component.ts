import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IGlobalConfigurationService, IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration';
import { BuildInfo } from '@app-shared-contracts/models/build-info.model';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent extends AppBaselineComponent implements OnInit {

  public loginForm: FormGroup;

  public version = '';
  public buildInfo: BuildInfo;
  public errorMessage = '';

  public constructor(
    private readonly formBuilder: FormBuilder,

    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService,
    @Inject(IGlobalConfigurationServiceToken) private readonly srvGlobalConfiguration: IGlobalConfigurationService
  ) {
    super();
  }

  public ngOnInit(): void {
    this.version = this.srvGlobalConfiguration.version;
    this.buildInfo = this.srvGlobalConfiguration.buildInfo;

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public tryLoginAsync = async (email: string, password: string): Promise<void> => {
    try {
      await this.srvAuth.signInAsync(email, password);
      this.srvNavigation.toDashboardAsync(); //no point to await
    } catch (err) {
      console.error(err);
      this.errorMessage = err.message;
    }
  }

  public tryGoogleLoginAsync = async (): Promise<void> => {
    try {
      await this.srvAuth.googleAuthAsync();
      this.srvNavigation.toDashboardAsync(); //no point to await
    } catch (err) {
      console.error(err);
      this.errorMessage = err.message;
    }
  }

  public tryFacebookLoginAsync = async (): Promise<void> => {
    try {
      await this.srvAuth.facebookAuthAsync();
      this.srvNavigation.toDashboardAsync(); //no point to await
    } catch (err) {
      console.error(err);
      this.errorMessage = err.message;
    }
  }
}
