import { Component, Inject } from '@angular/core';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends AppBaselineComponent {

  public emailSent = false;

  public constructor(@Inject(IAuthServiceToken) private readonly srvAuth: IAuthService) {
    super();
  }

  public resetPasswordClickedAsync = async (email: string): Promise<void> => {
    await this.srvAuth.forgotPasswordAsync(email);
    this.emailSent = true;
  }
}
