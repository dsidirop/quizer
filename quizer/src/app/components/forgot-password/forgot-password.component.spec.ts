import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { ForgotPasswordComponent } from './forgot-password.component';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ForgotPasswordComponent],
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
