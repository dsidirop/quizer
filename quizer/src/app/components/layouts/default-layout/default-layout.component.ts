import { Component, Inject } from '@angular/core';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent extends AppBaselineComponent {

  public constructor(
    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService
  ) { super(); }

  public signOutClickedAsync = async (): Promise<void> => {
    await this.srvAuth.signOutAsync();
    this.srvNavigation.toSignInAsync();
  }
}
