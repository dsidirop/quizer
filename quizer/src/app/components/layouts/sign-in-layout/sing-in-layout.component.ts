import { Component } from '@angular/core';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
    selector: 'app-sign-in-layout',
    template: '<router-outlet></router-outlet>'
})
export class SignInLayoutComponent extends AppBaselineComponent {
}
