import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IAuthServiceToken } from '@app-shared-contracts/services';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { NavigationServiceMock } from '@app/testing/mocks/navigation.service.mock';
import { SignUpComponent } from './sign-up.component';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        SignUpComponent
      ],
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: INavigationServiceToken, useClass: NavigationServiceMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
