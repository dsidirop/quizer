import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent extends AppBaselineComponent implements OnInit {

  public registerForm: FormGroup;
  public errorMessage = '';
  public successMessage = '';

  public constructor(
    private readonly formBuilder: FormBuilder,

    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public tryRegisterAsync = async (email: string, password: string): Promise<void> => {
    try {
      await this.srvAuth.signUpAsync(email, password);
      this.errorMessage = '';
      this.successMessage = 'Your Account has been Created!';
      this.srvNavigation.toVerifyEmailAsync();
    } catch (err) {
      this.errorMessage = err.message;
      this.successMessage = '';
    }
  }

  public tryGoogleLoginAsync = async (): Promise<void> => {
    try {
      await this.srvAuth.googleAuthAsync();
      this.srvNavigation.toDashboardAsync();
    } catch (err) {
      this.errorMessage = err.message;
      this.successMessage = '';
    }
  }

  public tryFacebookLoginAsync = async (): Promise<void> => {
    try {
      await this.srvAuth.facebookAuthAsync();
      this.srvNavigation.toDashboardAsync();
    } catch (err) {
      this.errorMessage = err.message;
      this.successMessage = '';
    }
  }
}
