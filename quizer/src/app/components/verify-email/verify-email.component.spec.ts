/* eslint-disable @typescript-eslint/no-unused-vars */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { VerifyEmailComponent } from './verify-email.component';

describe('VerifyEmailComponent', () => {
  let fixture: ComponentFixture<VerifyEmailComponent>;
  let component: VerifyEmailComponent;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [VerifyEmailComponent],
        providers: [
          { provide: IAuthServiceToken, useClass: AuthServiceMock },
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
