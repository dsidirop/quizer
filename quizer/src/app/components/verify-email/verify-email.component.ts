import { Component, Inject, OnInit } from '@angular/core';
import { UserData } from '@app-shared-contracts/models/user-data.model';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';
import { Observable } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent extends AppBaselineComponent implements OnInit {

  public userData$: Observable<UserData>;
  public errorMessage: string;

  public constructor(@Inject(IAuthServiceToken) private readonly srvAuth: IAuthService) {
    super();
  }

  public ngOnInit(): void {
    this.userData$ = this.srvAuth
      .authStateListener()
      .pipe(
        first(), //snapshot
        takeUntil(this.ngUnsubscribe$)
      );
  }

  public resendVerificationMailForCurrentUserAsync = async (): Promise<void> => {
    try {
      await this.srvAuth.sendVerificationMailForCurrentUserAsync();
    } catch (err) {
      this.errorMessage = err.message;
    }
  };
}
