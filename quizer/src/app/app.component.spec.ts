import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { AppComponent } from './app.component';
import { AppLocalStorageServiceMock } from './testing/mocks/app-local-storage.service.mock';
import { NavigationServiceMock } from './testing/mocks/navigation.service.mock';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: INavigationServiceToken, useClass: NavigationServiceMock },
        { provide: IAppLocalStorageServiceToken, useClass: AppLocalStorageServiceMock }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  // it('should have as title \'quizer\'', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   expect(app.title).toEqual('quizer');
  // });

  // it('should render title', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.nativeElement;
  //   expect(compiled.querySelector('.content span').textContent).toContain('quizer app is running!');
  // });
});
