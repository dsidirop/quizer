import { Component, Inject, OnInit } from '@angular/core';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { IAppLocalStorageService, IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //styleUrls: ['./app.component.scss']
})
export class AppComponent extends AppBaselineComponent implements OnInit {
  public title = 'Quizer';

  public constructor(
    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService,
    @Inject(IAppLocalStorageServiceToken) private readonly srvAppLocalStorage: IAppLocalStorageService
  ) { super(); }

  public ngOnInit(): void {
    this
      .srvAuth
      .authStateListener()
      .subscribe(userData => {
        this.srvAppLocalStorage.setUserData(userData);

        if (userData === null) {
          this.srvNavigation.toSignInAsync(); //0 auto log out
        }
      });

    //0 we navigate to the signin page in all tabs of the same browser   this doesnt have an effect
    //  in firefox vs chrome scenarios
  }

}
