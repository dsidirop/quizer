import { InjectionToken } from '@angular/core';
import { UserData } from '@app-shared-contracts/models';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';
import { Observable } from 'rxjs';

export const IUsersServiceToken = new InjectionToken<IUsersService>('app.usersservice');

export interface IUsersService {
  getUsersTidbits(uid: string): Observable<UserTidbits>;
  updateUserDataAsync(userData: Partial<UserData>): Promise<void>;
  updateUserTidbitsAsync(userTidbits: Partial<UserTidbits>): Promise<void>;
  usersTidbitsChangesListener(uid: string): Observable<UserTidbits>;
}
