import { InjectionToken } from '@angular/core';
import { UserData } from '@app-shared-contracts/models';
import { Observable } from 'rxjs';

export const IAuthServiceToken = new InjectionToken<IAuthService>('app.authservice');

export interface IAuthService {

  readonly isLoggedIn: boolean;
  authStateListener(): Observable<UserData>;

  signOutAsync(): Promise<void>;
  signInAsync(email: string, password: string): Promise<void>;
  signUpAsync(email: string, password: string): Promise<void>;

  googleAuthAsync(): Promise<void>;
  facebookAuthAsync(): Promise<void>;
  forgotPasswordAsync(passwordResetEmail: string): Promise<void>;

  sendVerificationMailForCurrentUserAsync(): Promise<void>;

}
