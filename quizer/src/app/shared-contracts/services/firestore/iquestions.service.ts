import { InjectionToken } from '@angular/core';
import { Question } from '@app-shared-contracts/models';
import { Observable } from 'rxjs';

export const IQuestionsServiceToken = new InjectionToken<IQuestionsService>('app.questionsservice');

export interface IQuestionsService {

  getQuestions(page: number, pageSize: number): Observable<Question[]>;

}

