import { InjectionToken } from '@angular/core';
import { StorageService } from 'ngx-webstorage-service';
import { IAppGenericStorageService } from './iapp-generic-storage-base.service';

export const IAppGenericStorageServiceFactoryToken = new InjectionToken<IAppGenericStorageServiceFactory>('app.appstorageservicefactory');

export interface IAppGenericStorageServiceFactory {

  spawn(storage: StorageService, namespace: string): IAppGenericStorageService;

}
