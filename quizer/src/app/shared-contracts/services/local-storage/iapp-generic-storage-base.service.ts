import { InjectionToken } from '@angular/core';

export const IAppGenericStorageServiceToken = new InjectionToken<IAppGenericStorageService>('app.appstorageservice');

export interface IAppGenericStorageService {
  remove(key: string): IAppGenericStorageService;
  get<T extends unknown>(key: string): T;
  set<T extends unknown>(key: string, value: T): IAppGenericStorageService;
}

