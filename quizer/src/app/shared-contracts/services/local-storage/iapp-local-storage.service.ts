import { InjectionToken } from '@angular/core';
import { UserData } from '@app-shared-contracts/models';

export const IAppLocalStorageServiceToken = new InjectionToken<IAppLocalStorageService>('app.applocalstorageservice');

export interface IAppLocalStorageService {

  getUserData(): UserData;
  deleteUserData(): IAppLocalStorageService;
  setUserData(newValue: UserData): IAppLocalStorageService;

}
