import { PossibleAnswer, Question, UserData } from '@app-shared-contracts/models';
import { RawQuestion } from '@app-shared-contracts/models/raw/raw-question.model';
import { User } from 'firebase';
import { InjectionToken } from '@angular/core';

export const IMapperServiceToken = new InjectionToken<IMapperService>('app.mapperservice');

export interface IMapperService {

  converter1<IN extends RawQuestion[], OUT extends Question[]>(rawQuestions: IN): OUT;
  converter2<IN extends RawQuestion, OUT extends Question>(rawQuestion: IN): OUT;
  converter3<IN extends string[], OUT extends PossibleAnswer[]>(rawQuestions: IN): OUT;
  converter4<IN extends string, OUT extends PossibleAnswer>(i: number, rawPossibleAnswer: IN): OUT;
  converter5<IN extends User, OUT extends UserData>(user: IN): OUT;

}
