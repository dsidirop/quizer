import { InjectionToken } from '@angular/core';

export const INavigationServiceToken = new InjectionToken<INavigationService>('app.navigationservice');

export interface INavigationService {

  toSignInAsync(): Promise<boolean>;
  toDashboardAsync(): Promise<boolean>;
  toVerifyEmailAsync(): Promise<boolean>;

}
