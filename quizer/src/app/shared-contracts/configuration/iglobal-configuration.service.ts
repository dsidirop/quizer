import { InjectionToken } from '@angular/core';
import { FirebaseOptions } from '@angular/fire';
import { BuildInfo } from '@app-shared-contracts/models/build-info.model';
import { IGlobalConfiguration } from '@app-shared-contracts/models/iglobal-configuration.model';

export const IGlobalConfigurationServiceToken = new InjectionToken<IGlobalConfigurationService>('app.globalconfigurationservice');

export interface IGlobalConfigurationService extends IGlobalConfiguration {
  readonly globalConfiguration: IGlobalConfiguration;

  readonly version: string;
  readonly buildInfo: BuildInfo;
  readonly production: boolean;
  readonly ajaxRetries: number;
  readonly firebaseConfig: FirebaseOptions;
  readonly defaultTimeToAnswer: number;
  readonly localStorageNamespace: string;
  readonly questionsCurrentBatchSize: number;

  updateAll(newConfiguration: IGlobalConfiguration): void;
}
