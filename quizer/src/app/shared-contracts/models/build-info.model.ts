export interface BuildInfo {
  number: string;
  branch: string;
  unixTimestamp: string;
  commitShortSha: string;
}
