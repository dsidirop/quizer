export interface RawQuestion {
    id: string;
    description: string;
    timeToAnswer?: number;
    correctAnswer: number;
    possibleAnswers: string[];
}
