export interface UserTidbits {
    uid: string;
    questionsPageIndex: number;

    totalNumberOfQuestions: number;
    numberOfQuestionsAnsweredCorrectly: number;
}
