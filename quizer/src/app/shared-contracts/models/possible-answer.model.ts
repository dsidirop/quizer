export interface PossibleAnswer {
    id: number;
    text: string;
}
