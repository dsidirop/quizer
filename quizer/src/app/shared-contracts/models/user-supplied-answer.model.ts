export interface UserSuppliedAnswer {
    questionId: string;
    suppliedAnswerId: number;
}
