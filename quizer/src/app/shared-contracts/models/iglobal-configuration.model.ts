import { FirebaseOptions } from '@angular/fire';
import { BuildInfo } from './build-info.model';

export interface IGlobalConfiguration {
  production?: boolean;

  version: string;
  buildInfo: BuildInfo;

  ajaxRetries?: number;

  defaultTimeToAnswer?: number;
  localStorageNamespace?: string;
  questionsCurrentBatchSize?: number;

  firebaseConfig: FirebaseOptions;
}
