import { PossibleAnswer } from './possible-answer.model';

export interface Question {
  id: string;
  description: string;
  timeToAnswer?: number;
  correctAnswer: number;
  possibleAnswers: PossibleAnswer[];
}
