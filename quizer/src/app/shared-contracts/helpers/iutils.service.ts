import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export const IUtilsServiceToken = new InjectionToken<IUtilsService>('app.utilsservice');

export interface IUtilsService {
  readonly isBrowser: boolean;

  nameof<T>(key: keyof T, _instance?: T): keyof T;
  ensurePostfix(input: string, postfix: string);
  stripUndefinedProps<T>(object: T): T;
  observableFromFunction$<T>(factory: () => T): Observable<T>;
}
