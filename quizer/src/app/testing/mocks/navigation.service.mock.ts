import { INavigationService } from '@app-shared-contracts/services/navigation/inavigation.service';
import { of } from 'rxjs';

export class NavigationServiceMock implements INavigationService {
  public toSignInAsync = async (): Promise<boolean> => of<boolean>(true).toPromise<boolean>();
  public toDashboardAsync = async (): Promise<boolean> => of<boolean>(true).toPromise<boolean>();
  public toVerifyEmailAsync = async (): Promise<boolean> => of<boolean>(true).toPromise<boolean>();
}
