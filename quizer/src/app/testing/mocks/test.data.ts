import { UserData } from '@app-shared-contracts/models';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';

export const DummyUserData: UserData = {
  uid: '123',
  email: 'foo@mail.com',
  photoURL: 'https://foobar.com/abcz',
  displayName: 'John Smith',
  emailVerified: true
};

export const DummyUserTidbits: UserTidbits = {
  uid: DummyUserData.uid,
  questionsPageIndex: 1,

  totalNumberOfQuestions: 10,
  numberOfQuestionsAnsweredCorrectly: 2
};
