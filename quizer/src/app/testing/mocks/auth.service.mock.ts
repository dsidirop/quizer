/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserData } from '@app-shared-contracts/models';
import { IAuthService } from '@app-shared-contracts/services/firestore/iauth.service';
import { Observable, of } from 'rxjs';
import { DummyUserData } from './test.data';

export class AuthServiceMock implements IAuthService {

  public get isLoggedIn(): boolean { return true; }

  public authStateListener = (): Observable<UserData> => of<UserData>(DummyUserData);

  public signInAsync = async (email: string, password: string): Promise<void> => of<void>().toPromise<void>();
  public signUpAsync = async (email: string, password: string): Promise<void> => of<void>().toPromise<void>();
  public signOutAsync = async (): Promise<void> => of<void>().toPromise<void>();
  public googleAuthAsync = async (): Promise<void> => of<void>().toPromise<void>();
  public facebookAuthAsync = async (): Promise<void> => of<void>().toPromise<void>();
  public forgotPasswordAsync = async (passwordResetEmail: string): Promise<void> => of<void>().toPromise<void>();
  public sendVerificationMailForCurrentUserAsync = async (): Promise<void> => of<void>().toPromise<void>();

}
