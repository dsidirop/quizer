/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserData } from '@app-shared-contracts/models/user-data.model';
import { IAppLocalStorageService } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { DummyUserData } from './test.data';

export class AppLocalStorageServiceMock implements IAppLocalStorageService {

  private readonly dummyStore = new Map<string, unknown>();

  public constructor() {
    this.setUserData(DummyUserData);
  }

  deleteUserData = (): IAppLocalStorageService => {
    this.dummyStore.delete('user');
    return this;
  }

  public getUserData = (): UserData => this.dummyStore.get('user') as UserData;

  public setUserData = (newValue: UserData): IAppLocalStorageService => {
    this.dummyStore.set('user', newValue);
    return this;
  }

}
