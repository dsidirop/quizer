import { StorageService, StorageTranscoder } from 'ngx-webstorage-service';

export class StorageServiceMock implements StorageService {
  private readonly dummyStore = new Map<string, unknown>();
  public has = (key: string): boolean => this.dummyStore.has(key);
  public remove = (key: string): void => { this.dummyStore.delete(key); };
  public clear = (): void => this.dummyStore.clear();
  public withDefaultTranscoder = <X>(transcoder: StorageTranscoder<X>): StorageService<X> => { throw new Error('Method not implemented.'); };
  public get<T>(key: any, decoder?: any): T { return this.dummyStore.get(key) as T; }
  public set(key: any, value: any, encoder?: any): void { this.dummyStore.set(key, value); }
}
