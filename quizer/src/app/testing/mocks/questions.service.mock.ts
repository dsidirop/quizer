/* eslint-disable @typescript-eslint/no-unused-vars */
import { PossibleAnswer, Question } from '@app-shared-contracts/models';
import { IQuestionsService } from '@app-shared-contracts/services/firestore/iquestions.service';
import { Observable, of } from 'rxjs';

export class QuestionsServiceMock implements IQuestionsService {
  public getQuestions = (page: number, pageSize: number): Observable<Question[]> => {

    const q1: Question = {
      id: 'abc',
      description: 'Foobar',
      timeToAnswer: 10,
      correctAnswer: 1,
      possibleAnswers: [
        {
          id: 0,
          text: 'Foo 1'
        } as PossibleAnswer,
        {
          id: 1,
          text: 'Foo 2'
        } as PossibleAnswer
      ]
    };

    return of([q1]);
  };
}

