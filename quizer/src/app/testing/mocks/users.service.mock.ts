/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserData } from '@app-shared-contracts/models';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';
import { IUsersService } from '@app-shared-contracts/services/firestore/iusers.service';
import { Observable, of } from 'rxjs';
import { DummyUserTidbits } from './test.data';

export class UsersServiceMock implements IUsersService {
  public getUsersTidbits = (uid: string): Observable<UserTidbits> => of(DummyUserTidbits);
  public updateUserDataAsync = (userData: Partial<UserData>): Promise<void> => of<void>().toPromise();
  public updateUserTidbitsAsync = (userTidbits: Partial<UserTidbits>): Promise<void> => of<void>().toPromise();
  public usersTidbitsChangesListener = (uid: string): Observable<UserTidbits> => of(DummyUserTidbits);
}
