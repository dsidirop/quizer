import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlreadyLoggedInGuard as AlreadyLoggedInGuard } from '@app-shared/guard/already-logged-in.guard';
import { AuthGuard } from '@app-shared/guard/auth.guard';
import { DashboardComponent } from '@app/components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from '@app/components/forgot-password/forgot-password.component';
import { NewQuizComponent } from '@app/components/new-quiz/new-quiz.component';
import { SignInComponent } from '@app/components/sign-in/sign-in.component';
import { SignUpComponent } from '@app/components/sign-up/sign-up.component';
import { VerifyEmailComponent } from '@app/components/verify-email/verify-email.component';
import { DefaultLayoutComponent } from '@app/components/layouts/default-layout';
import { SignInLayoutComponent } from '@app/components/layouts/sign-in-layout';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/sign-in'
    },
    {
        path: '',
        component: SignInLayoutComponent,
        canActivate: [AlreadyLoggedInGuard],
        children: [
            { path: 'sign-in', component: SignInComponent },
            { path: 'sign-up', component: SignUpComponent },
            { path: 'verify-email', component: VerifyEmailComponent },
            { path: 'forgot-password', component: ForgotPasswordComponent }
        ]
    },
    {
        path: '',
        component: DefaultLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'new-quiz',
                component: NewQuizComponent
            }
        ]
    },
    { path: '**', redirectTo: '/sign-in' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }