// we refrained from extending error class   more info
//
//    https://stackoverflow.com/a/41429145/863651
//    https://gunargessner.com/subclassing-exception
//
//    "instanceof is broken when class extends Error type"
//    https://github.com/Microsoft/TypeScript/issues/13965
//
// this issue is here to stay from the looks of it but its not that bad

export default class AppBaseException extends Error {
  public constructor(message: string) {
    super(message); //0
    Object.setPrototypeOf(this, AppBaseException.prototype); //0

    //0 setPrototypeOf() must be called immediately after super()   sets the prototype explicitly
  }
}
