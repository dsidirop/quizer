import { Injectable } from '@angular/core';
import { PossibleAnswer, Question, UserData } from '@app-shared-contracts/models';
import { RawQuestion } from '@app-shared-contracts/models/raw/raw-question.model';
import { IMapperService } from '@app-shared-contracts/services/mapper/imapper.service';
import { User } from 'firebase';

@Injectable()
export class MapperService implements IMapperService {

  public converter1 = <IN extends RawQuestion[], OUT extends Question[]>(rawQuestions: IN): OUT =>
    rawQuestions?.map(y => this.converter2(y)) as unknown as OUT;

  public converter2 = <IN extends RawQuestion, OUT extends Question>(rawQuestion: IN): OUT => (
    rawQuestion
      ? {
        id: rawQuestion.id,
        description: rawQuestion.description,
        timeToAnswer: rawQuestion.timeToAnswer,
        correctAnswer: rawQuestion.correctAnswer,
        possibleAnswers: this.converter3(rawQuestion.possibleAnswers)
      } as unknown as OUT
      : null
  );

  public converter3 = <IN extends string[], OUT extends PossibleAnswer[]>(rawQuestions: IN): OUT =>
    rawQuestions?.map((y, i) => this.converter4(i, y)) as unknown as OUT;

  public converter4 = <IN extends string, OUT extends PossibleAnswer>(i: number, rawPossibleAnswer: IN): OUT => ({
    id: i,
    text: rawPossibleAnswer
  } as unknown as OUT);

  public converter5 = <IN extends User, OUT extends UserData>(user: IN): OUT => (
    user
      ? {
        uid: user.uid,
        email: user.email,
        photoURL: user.photoURL,
        displayName: user.displayName,
        emailVerified: user.emailVerified
      } as unknown as OUT
      : null
  );

}
