import { Inject, Injectable } from '@angular/core';
import { FirebaseOptions } from '@angular/fire';
import { environment } from '@app-env/environment';
import { IGlobalConfigurationService } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { IUtilsService, IUtilsServiceToken } from '@app-shared-contracts/helpers/iutils.service';
import { BuildInfo } from '@app-shared-contracts/models/build-info.model';
import { IGlobalConfiguration } from '@app-shared-contracts/models/iglobal-configuration.model';

// medium.com/@seangwright/the-best-way-to-use-angulars-environment-files-a0c098551abc

@Injectable()
export class GlobalConfigurationService implements IGlobalConfigurationService {
  private g: IGlobalConfiguration;

  public constructor(@Inject(IUtilsServiceToken) private readonly utils: IUtilsService) {
    this.g = this.sanitizeConfiguration(environment);
  }

  public get globalConfiguration(): IGlobalConfiguration { return this.g; }

  public get version(): string { return this.g.version; }
  public get buildInfo(): BuildInfo { return this.g.buildInfo; }
  public get production(): boolean { return this.g.production; }
  public get ajaxRetries(): number { return this.g.ajaxRetries; }
  public get firebaseConfig(): FirebaseOptions { return this.g.firebaseConfig; }
  public get defaultTimeToAnswer(): number { return this.g.defaultTimeToAnswer; }
  public get localStorageNamespace(): string { return this.g.localStorageNamespace; }
  public get questionsCurrentBatchSize(): number { return this.g.questionsCurrentBatchSize; }

  public updateAll = (newConfiguration: IGlobalConfiguration): void => {
    this.g = this.sanitizeConfiguration(newConfiguration);
  }

  private sanitizeConfiguration = (newConfiguration: IGlobalConfiguration): IGlobalConfiguration => {

    const sanitizeBuildInfo = (bi: BuildInfo): BuildInfo => {
      const result = { ...bi } as BuildInfo;

      result.number = !result.number || result.number.startsWith('#{') ? '1000' : result.number;
      result.branch = !result.branch || result.branch.startsWith('#{') ? 'develop' : result.branch;
      result.unixTimestamp = !result.unixTimestamp || result.unixTimestamp.startsWith('#{') ? '1591945553' : result.unixTimestamp;
      result.commitShortSha = !result.commitShortSha || result.commitShortSha.startsWith('#{') ? 'aaaaaaaa' : result.commitShortSha;

      return result;
    }

    const {
      version,
      buildInfo,
      production,
      ajaxRetries,
      firebaseConfig,
      defaultTimeToAnswer,
      localStorageNamespace,
      questionsCurrentBatchSize
    } = newConfiguration;

    const g = {} as IGlobalConfiguration;
    g.version = version || 'N/A';
    g.buildInfo = sanitizeBuildInfo(buildInfo);
    g.production = production ?? true;
    g.ajaxRetries = ajaxRetries ?? 1;
    g.firebaseConfig = firebaseConfig;
    g.defaultTimeToAnswer = defaultTimeToAnswer ?? 10;
    g.localStorageNamespace = localStorageNamespace;
    g.questionsCurrentBatchSize = questionsCurrentBatchSize ?? 5;

    return g;

    //0 we append a trailing slash to play it safe just in case
    //1 we refetch all drivers and their respective race performance once every twenty four hours
    //  it makes sense considering
    //
    //  - new drivers come about mainly at the very beginning of a new f1 season
    //  - their most recent race performances on f1 take a few hours to be included in the db
  }
}
