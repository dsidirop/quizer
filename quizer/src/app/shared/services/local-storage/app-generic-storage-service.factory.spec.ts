/* eslint-disable @typescript-eslint/no-unused-vars */
import { async, inject, TestBed } from '@angular/core/testing';
import { IAppGenericStorageServiceFactory, IAppGenericStorageServiceFactoryToken } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-service.factory';
import { StorageServiceMock } from '@app/testing/mocks/storage.service.mock';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AppGenericStorageServiceFactory } from './app-generic-storage-service.factory';

describe('AppGenericStorageServiceFactory', () => {
  let factory: IAppGenericStorageServiceFactory;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: LOCAL_STORAGE, useClass: StorageServiceMock },
        { provide: IAppGenericStorageServiceFactoryToken, useClass: AppGenericStorageServiceFactory }
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    factory = TestBed.inject(IAppGenericStorageServiceFactoryToken);
  });

  it('should be created', () => {
    expect(factory).toBeTruthy();
  });

  it('should spawn generic storage service instance', inject([LOCAL_STORAGE], (localStorage: StorageService) => {
    expect(factory.spawn(localStorage, 'abc')).toBeTruthy();
  }));

});
