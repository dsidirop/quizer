import { Inject, Injectable } from '@angular/core';
import { IGlobalConfigurationService, IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { UserData } from '@app-shared-contracts/models';
import { ELocalStorageKeys } from '@app-shared-contracts/models/enums';
import { IAppGenericStorageService } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-base.service';
import { IAppGenericStorageServiceFactory, IAppGenericStorageServiceFactoryToken } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-service.factory';
import { IAppLocalStorageService } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable()
export class AppLocalStorageService implements IAppLocalStorageService {
  private readonly srvAppStorageBase: IAppGenericStorageService;

  public constructor(
    @Inject(LOCAL_STORAGE) srvLocalStorage: StorageService,
    @Inject(IGlobalConfigurationServiceToken) srvGlobalConfiguration: IGlobalConfigurationService,
    @Inject(IAppGenericStorageServiceFactoryToken) srvAppGenericStorageServiceFactory: IAppGenericStorageServiceFactory
  ) {
    this.srvAppStorageBase = srvAppGenericStorageServiceFactory.spawn(srvLocalStorage, srvGlobalConfiguration.localStorageNamespace);
  }

  public getUserData = (): UserData => {
    return this.srvAppStorageBase.get<UserData>(ELocalStorageKeys.User);
  }

  public setUserData = (newValue: UserData): IAppLocalStorageService => {
    this.srvAppStorageBase.set<UserData>(ELocalStorageKeys.User, newValue);
    return this;
  }

  public deleteUserData = (): IAppLocalStorageService => {
    this.srvAppStorageBase.remove(ELocalStorageKeys.User);
    return this;
  }
}
