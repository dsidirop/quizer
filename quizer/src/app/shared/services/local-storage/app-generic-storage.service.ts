import { IAppGenericStorageService } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-base.service'
import { trim as _trim } from 'lodash'
import { StorageService } from 'ngx-webstorage-service'

// meant to be used via its factory   never inject this directly

export class AppGenericStorageService implements IAppGenericStorageService {

  public constructor(
    private readonly storage: StorageService, //usually localstorage but it can also be sessionstorage
    private readonly sanitizedBaseNamespace?: string
  ) {
    this.sanitizedBaseNamespace = AppGenericStorageService.sanitizeKey(this.sanitizedBaseNamespace)
  }

  public remove = (key: string): IAppGenericStorageService => {
    this.storage.remove(this.getNamespacedKey(key));
    return this;
  }

  public get = <T extends unknown>(key: string): T => {
    return this.storage.get(this.getNamespacedKey(key)); //0 json

    //0 it seems that the underlying storage object takes care of seamlessly serializing
    //  and deserializing objects   so dont worry about json strings here
  }

  public set = <T extends unknown>(key: string, value: T): IAppGenericStorageService => {
    this.storage.set(this.getNamespacedKey(key), value); //0
    return this;

    //0 it seems that the underlying storage object takes care of seamlessly serializing
    //  and deserializing objects   so dont worry about json strings here
  }

  private getNamespacedKey = (key: string): string => {
    return [
      this.sanitizedBaseNamespace, //order
      AppGenericStorageService.sanitizeKey(key) //order
    ]
      .filter(x => x) //0
      .join('.');

    //0 if the base namespace is not set we obviously want to omit it along with the dot after it
  }

  private static sanitizeKey = (key: string): string => {
    return _trim(key, '\r\n \t.'); // '  . . .  foo  .. . . .  '   ->   'foo'
  }

}
