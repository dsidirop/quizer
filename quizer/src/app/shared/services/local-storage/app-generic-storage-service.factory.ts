import { IAppGenericStorageService } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-base.service';
import { IAppGenericStorageServiceFactory } from '@app-shared-contracts/services/local-storage/iapp-generic-storage-service.factory';
import { StorageService } from 'ngx-webstorage-service';
import { AppGenericStorageService } from './app-generic-storage.service';

export class AppGenericStorageServiceFactory implements IAppGenericStorageServiceFactory {

  public spawn = (storage: StorageService, namespace: string): IAppGenericStorageService =>
    new AppGenericStorageService(storage, namespace)

}
