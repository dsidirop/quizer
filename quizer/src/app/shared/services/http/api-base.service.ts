import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { IGlobalConfigurationService, IGlobalConfigurationServiceToken } from '@app-shared-contracts/configuration/iglobal-configuration.service';
import { ResourceNotFoundException, UnexpectedHttpErrorException } from '@app-shared/exceptions';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';

@Injectable()
export abstract class ApiBaseService {

  public constructor(
    private readonly httpClient: HttpClient,

    @Inject(IGlobalConfigurationServiceToken) private readonly srvGlobalConfiguration: IGlobalConfigurationService
  ) {
  }

  protected abstract backendUrl(): string; // we dont cache these two on purpose
  protected ajaxRetries(): number { return this.srvGlobalConfiguration.ajaxRetries; } //  if we do we will break the autoupdate mechanism

  public get<T>(path: string): Observable<T> {
    return this.httpClient
      .get(`${this.backendUrl()}${path}`, this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public put<T>(path: string, body: unknown = {}): Observable<T> {
    return this.httpClient
      .put(`${this.backendUrl()}${path}`, JSON.stringify(body), this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public post<T>(path: string, body: unknown = {}): Observable<T> {
    return this.httpClient
      .post(`${this.backendUrl()}${path}`, JSON.stringify(body), this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public delete<T>(path: string): Observable<T> {
    return this.httpClient
      .delete(`${this.backendUrl()}${path}`, this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  private spawnHeaders = (): { headers: HttpHeaders } => ({ headers: new HttpHeaders(ApiBaseService.HeadersConfig) })

  private genericErrorHandler = (error: HttpErrorResponse | unknown): Observable<never> => {
    const asHttpErrorResponse = error as HttpErrorResponse;
    if (!asHttpErrorResponse) {
      return throwError(new UnexpectedHttpErrorException(`[AB.GEH01] Unintelligible Error: '${JSON.stringify(error)}'`));
    }

    if (asHttpErrorResponse.status === 404) {
      return throwError(new ResourceNotFoundException(asHttpErrorResponse.message)); //special handling
    }

    return throwError(
      asHttpErrorResponse.error instanceof ErrorEvent
        ? asHttpErrorResponse.error?.message
        : `Error Code: ${asHttpErrorResponse.status}\nMessage: ${asHttpErrorResponse.message}`
    );
  }

  private static readonly HeadersConfig = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
}
