import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { IUtilsService } from '@app-shared-contracts/helpers/iutils.service';
import { curry as _curry, deburr as _deburr, endsWith as _endsWith, flow as _flow, isUndefined as _isUndefined, omitBy as _omitBy, trim as _trim, upperCase as _upperCase } from 'lodash';
import { Observable, Subscriber } from 'rxjs';

@Injectable()
export class UtilsService implements IUtilsService {
  public readonly isBrowser: boolean;

  public constructor(@Inject(PLATFORM_ID) platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  public stripUndefinedProps = <T>(object: T): T => _omitBy(object, _isUndefined);

  public observableFromFunction$ = <T>(factory: () => T): Observable<T> => { //0
    return Observable.create((observer: Subscriber<T>) => {
      try {
        observer.next(factory());
      } catch (error) {
        observer.error(error);
      } finally {
        observer.complete();
      }
    });

    //0 stackoverflow.com/a/45114028/863651
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public nameof = <T>(key: keyof T, _instance?: T): keyof T => key;

  public ensurePostfix = (input: string, postfix: string): string =>
    this.f.stringEnsurePostfix(_trim(input), postfix);

  private readonly f = {
    stringContains: _curry((x: string, y: string) => x.indexOf(y) !== -1),
    stringEnsurePostfix: _flow(
      (s: string, p: string) => ({ e: _endsWith(s, p), s, p }),
      ({ e, s, p }: { e: boolean, s: string, p: string }) => (e ? s : s + p)
    ),
    stringLocaleCompare: _curry((x: string, y: string) => x.localeCompare(y)),
    stringDeburrAndUpperCase: _flow(_deburr, _upperCase, _trim)
  };
}
