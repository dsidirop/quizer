import { Inject, Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserData } from '@app-shared-contracts/models';
import { IAuthService } from '@app-shared-contracts/services/firestore/iauth.service';
import { IUsersService, IUsersServiceToken } from '@app-shared-contracts/services/firestore/iusers.service';
import { IAppLocalStorageService, IAppLocalStorageServiceToken } from '@app-shared-contracts/services/local-storage/iapp-local-storage.service';
import { IMapperService, IMapperServiceToken } from '@app-shared-contracts/services/mapper/imapper.service';
import { auth, User } from 'firebase/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService implements IAuthService {

  public constructor(
    private readonly angularFireAuth: AngularFireAuth,

    @Inject(IUsersServiceToken) private readonly srvUsers: IUsersService,
    @Inject(IMapperServiceToken) private readonly srvAutomapper: IMapperService,
    @Inject(IAppLocalStorageServiceToken) private readonly srvAppLocalStorage: IAppLocalStorageService
  ) {
  }

  public get isLoggedIn(): boolean {
    return this.srvAppLocalStorage.getUserData()?.emailVerified ?? false; //0

    //0 for the user to be considered logged in his email must be verified first
  }

  public authStateListener = (): Observable<UserData> => this
    .angularFireAuth
    .authState
    .pipe(
      map(x => this.srvAutomapper.converter5<User, UserData>(x))
    );

  public signInAsync = async (email: string, password: string): Promise<void> => {
    const result = await this.angularFireAuth.signInWithEmailAndPassword(email, password);
    const userData = this.srvAutomapper.converter5(result.user);
    await this.srvUsers.updateUserDataAsync(userData);
  }

  public signUpAsync = async (email: string, password: string): Promise<void> => {
    const result = await this.angularFireAuth.createUserWithEmailAndPassword(email, password);
    const userData = this.srvAutomapper.converter5(result.user);
    await this.srvUsers.updateUserDataAsync(userData);
    await this.sendVerificationMailForCurrentUserAsync();
  }

  public sendVerificationMailForCurrentUserAsync = async (): Promise<void> => {
    const currentUser = await this.angularFireAuth.currentUser;
    await currentUser.sendEmailVerification();
  }

  public googleAuthAsync = async (): Promise<void> => await this.authLoginAsync(new auth.GoogleAuthProvider());
  public facebookAuthAsync = async (): Promise<void> => await this.authLoginAsync(new auth.FacebookAuthProvider());
  public forgotPasswordAsync = async (passwordResetEmail: string): Promise<void> => await this.angularFireAuth.sendPasswordResetEmail(passwordResetEmail);

  public signOutAsync = async (): Promise<void> => {
    try {
      await this.angularFireAuth.signOut(); //0
    } catch (err) {
      console.error(err);
    }
    this.srvAppLocalStorage.setUserData(null); //1 keep this

    //0 if the user clears the cache of the browser manually then we will get an error here along the lines of
    //
    //    DOMException Failed to execute 'transaction' on 'IDBDatabase' The database connection is closing
    //
    //  its safe to suppress it and move on
    //
    //1 keep setting the userdata to null explicitly here   we are aware that we also have a listener via
    //  wireUpUserDataLocalStorageAutoUpdate however it might not get fired if the call to signout throws
    //  an exception   read the comment above about this cornercase
  }

  private authLoginAsync = async (provider: auth.AuthProvider): Promise<void> => {
    const result = await this.angularFireAuth.signInWithPopup(provider);
    if (!result?.user)
      throw new Error('[AS.ALA01] [BUG] Failed to get authed user!');

    const userData = this.srvAutomapper.converter5(result.user);
    if (!userData.emailVerified) {
      await this.sendVerificationMailForCurrentUserAsync();
      throw new Error('You need to verify the email of this account before you can login! Check your Email then try again!');
    }

    await this.srvUsers.updateUserDataAsync(userData);
  }

}
