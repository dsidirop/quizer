import { Inject, Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IUtilsService, IUtilsServiceToken } from '@app-shared-contracts/helpers/iutils.service';
import { UserData } from '@app-shared-contracts/models/user-data.model';
import { UserTidbits } from '@app-shared-contracts/models/user-tidbits.model';
import { IUsersService } from '@app-shared-contracts/services/firestore/iusers.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UsersService implements IUsersService {

  public constructor(
    private readonly store: AngularFirestore,

    @Inject(IUtilsServiceToken) private readonly srvUtils: IUtilsService
  ) {
  }

  public updateUserDataAsync = async (userData: Partial<UserData>): Promise<void> => {
    if (!userData?.uid)
      throw new Error('[US.UUDA01] [BUG] uid not set!');

    const payload = this.srvUtils.stripUndefinedProps(userData);

    return await this
      .store
      .doc(`users/${userData.uid}`)
      .set(payload, { merge: true });

    // we are setting up user data when signing-in with username-password or sign-up-with-username-password and
    // sign-in-with-social-auth-provider in firestore database using angular-firestore + angular-firestore-document service
  };

  public updateUserTidbitsAsync = async (userTidbits: Partial<UserTidbits>): Promise<void> => {
    if (!userTidbits?.uid)
      throw new Error('[US.UUTA01] [BUG] uid not set!');

    const payload = this.srvUtils.stripUndefinedProps(userTidbits);

    await this
      .store
      .doc(`usersTidbits/${userTidbits.uid}`)
      .set(payload, { merge: true });

    //0 undefined doesnt cut it for unset properties   we need to resort to null
  };

  public usersTidbitsChangesListener = (uid: string): Observable<UserTidbits> => this
    .store
    .collection<UserTidbits>('usersTidbits')
    .doc<UserTidbits>(uid)
    .valueChanges()
    .pipe(
      map(tidbits => this.mapTidbits(uid, tidbits))
    );

  public getUsersTidbits = (uid: string): Observable<UserTidbits> => this
    .store
    .collection<UserTidbits>('usersTidbits')
    .doc<UserTidbits>(uid)
    .get()
    .pipe(
      map(tidbits => this.mapTidbits(uid, tidbits.data() as UserTidbits))
    );

  private mapTidbits = (uid: string, tidbits: UserTidbits) => {
    return ( //null template essentially
      {
        uid: uid,
        questionsPageIndex: 0,
        totalNumberOfQuestions: 0,
        numberOfQuestionsAnsweredCorrectly: 0,
        ...tidbits
      } as UserTidbits
    );
  }
}
