import { Inject, Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Question } from '@app-shared-contracts/models';
import { RawQuestion } from '@app-shared-contracts/models/raw/raw-question.model';
import { IQuestionsService } from '@app-shared-contracts/services/firestore/iquestions.service';
import { IMapperService, IMapperServiceToken } from '@app-shared-contracts/services/mapper/imapper.service';
import { from as ixFrom } from 'ix/iterable';
import { filter as ixFilter, map as ixMap } from 'ix/iterable/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class QuestionsService implements IQuestionsService {

  public constructor(
    private readonly store: AngularFirestore,

    @Inject(IMapperServiceToken) private readonly mapper: IMapperService
  ) {
  }

  public getQuestions = (page: number, pageSize: number): Observable<Question[]> => {
    return this
      .store
      .collection(
        'questions',
        ref => ref.limit((page + 1) * pageSize) //temp hack

        // ,ref => ref
        //   .limit(count)
        //   .orderBy('timestamp', 'asc') //for some weird reason this causes the results list to be empty   but why?
        //   .startAfter(this.last)
      )
      .get()
      .pipe(
        map(response => {
          const offset = page * pageSize; //order
          const maxIndex = offset + pageSize; //order

          const results = ixFrom(response.docs) //transducer
            .pipe(
              ixFilter((_, i) => i >= offset && i <= maxIndex),
              ixMap(
                x => this.mapper.converter2<RawQuestion, Question>(
                  {
                    ...(x.data() as RawQuestion),
                    id: x.id //questionid
                  } as RawQuestion
                )
              )
            );

          return Array.from(results);
        })
      );
  };

}
