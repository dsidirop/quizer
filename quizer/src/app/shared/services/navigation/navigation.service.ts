import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { INavigationService } from '@app-shared-contracts/services/navigation/inavigation.service';

@Injectable()
export class NavigationService implements INavigationService {

  public constructor(private readonly router: Router) {
  }

  public toSignInAsync = async (): Promise<boolean> => await this.router.navigate(['sign-in']);
  public toDashboardAsync = async (): Promise<boolean> => await this.router.navigate(['/dashboard']);
  public toVerifyEmailAsync = async (): Promise<boolean> => await this.router.navigate(['/verify-email']);

}
