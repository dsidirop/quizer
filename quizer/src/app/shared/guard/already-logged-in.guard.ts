import { Inject, Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AlreadyLoggedInGuard implements CanActivate {

  public constructor(
    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService
  ) { }

  canActivate(
    // next: ActivatedRouteSnapshot,
    // state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (this.srvAuth.isLoggedIn) { //0
      this.srvNavigation.toDashboardAsync();
    }

    return true;

    //0 this will prevent access for sign in sign up password recovery and email verification pages
    //  when the user is already logged in
  }

}
