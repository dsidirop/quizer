import { TestBed } from '@angular/core/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { NavigationServiceMock } from '@app/testing/mocks/navigation.service.mock';
import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  let guard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: INavigationServiceToken, useClass: NavigationServiceMock }
      ],
    }).compileComponents();

    guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
