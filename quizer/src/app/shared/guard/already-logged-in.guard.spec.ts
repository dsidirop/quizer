import { TestBed } from '@angular/core/testing';
import { IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { AuthServiceMock } from '@app/testing/mocks/auth.service.mock';
import { NavigationServiceMock } from '@app/testing/mocks/navigation.service.mock';
import { AlreadyLoggedInGuard } from './already-logged-in.guard';

describe('AlreadyLoggedInGuard', () => {
  let guard: AlreadyLoggedInGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: IAuthServiceToken, useClass: AuthServiceMock },
        { provide: INavigationServiceToken, useClass: NavigationServiceMock }
      ],
    }).compileComponents();
    guard = TestBed.inject(AlreadyLoggedInGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
