import { Inject, Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { IAuthService, IAuthServiceToken } from '@app-shared-contracts/services/firestore/iauth.service';
import { INavigationService, INavigationServiceToken } from '@app-shared-contracts/services/navigation/inavigation.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  public constructor(
    @Inject(IAuthServiceToken) private readonly srvAuth: IAuthService,
    @Inject(INavigationServiceToken) private readonly srvNavigation: INavigationService
  ) { }

  public canActivate(
    // next: ActivatedRouteSnapshot,
    // state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.srvAuth.isLoggedIn) {
      this.srvNavigation.toSignInAsync();
    }

    return true;
  }
}
