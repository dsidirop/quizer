<img src="https://d33wubrfki0l68.cloudfront.net/49c2be6f2607b5c12dd27f8ecc8521723447975d/f05c5/logo-small.cbbeba89.svg" alt="Nest Js logo" title="Nest Js" height="96" width="96" align="right"/> <img src="https://storage.googleapis.com/gweb-cloudblog-publish/images/cloud_function_firebase.max-400x400.png" alt="Firebase Cloud Functions logo" title="Firebase Cloud Functions" height="96" width="96" align="right"/>

# NestJs Firebase Cloud Functions

 0. Generate a new service key. Pay a visit over to:
  
        quizer-dev    https://console.firebase.google.com/u/1/project/quizer-d6b60/settings/serviceaccounts/adminsdk
        quizer-prod   https://console.firebase.google.com/u/1/project/qu1z3r/settings/serviceaccounts/adminsdk

    Place the key into:

        quizer\functions\src\environments\firestore-service-account.info.sensitive.json
             (Make sure you don't commit this file in git by mistake though!)

    The structure should be:

```javascript
     {
       "databaseURL": "https://quizer-d6b60.firebaseio.com/",
       "certification": {
         "type": "service_account",
         "auth_uri": "https://accounts.google.com/o/oauth2/auth",
         "token_uri": "https://oauth2.googleapis.com/token",
         "client_id": "11...25",
         "project_id": "quizer-d6b60",
         "private_key": "-----BEGIN PRIVATE KEY-----wef...ov75\n-----END PRIVATE KEY-----\n",
         "client_email": "firebase-adminsdk-pgn9d@quizer-d6b60.iam.gserviceaccount.com",
         "private_key_id": "277...bf2",
         "client_x509_cert_url": "https://www.googleapis...unt.com",
         "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs"
       }
     }
```


1. Install all dependencies `npm install`
2. Do a local test with the firebase local server `npm run serve` so you can see that verything is fine. The localhost url should be something like:

      http://localhost:5000/quizer-d6b60/us-central1/api

3. Once you see it running without errors in your local environment, take the project to the server `npm run deploy`. The url should be something like:

      https://us-central1-quizer-d6b60.cloudfunctions.net/api

4. **Start the development of your back end using nestJs:** `npm run start:dev`
