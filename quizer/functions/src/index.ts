import { INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as functions from 'firebase-functions';
import { AppModule } from './nestApi/app.module';

const startNestApplication = async (expressInstance: express.Express): Promise<INestApplicationContext> => {
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(expressInstance)
  );

  return await app.init();
};

const server: express.Express = express()
  .use(cors())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }));

startNestApplication(server)
  .then(() => console.info('** [INFO] [Nest] NestJs Launched!'))
  .catch(err => console.error('** [ERROR] [Nest] NestJs Failed to Launch!', err));

export const api = functions.https.onRequest(server);
