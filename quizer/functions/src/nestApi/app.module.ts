// import { HttpClientModule } from '@angular/common/http';
// import { AngularFireModule } from '@angular/fire';
// import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
// import { BrowserModule } from '@angular/platform-browser';
import { Module } from '@nestjs/common';
import { QuestionsController } from './controllers/questions.controller';
import { AppFirebaseConfigurationService } from './services/app-firebase-configuration.service';
import { QuestionsService } from './services/questions.service';

@Module({
  imports: [
    // BrowserModule,
    // HttpClientModule,
    // AngularFireModule, // .initializeApp(environment.firebaseConfig),
    // AngularFireAuthModule,
    // AngularFirestoreModule
  ],
  providers: [
    // AngularFirestore
    QuestionsService,
    AppFirebaseConfigurationService
  ],
  controllers: [QuestionsController]
})
export class AppModule {
}
