import { Test, TestingModule } from '@nestjs/testing';
import { } from 'jasmine';
import { AppFirebaseConfigurationService } from '../services/app-firebase-configuration.service';
import { QuestionsService } from '../services/questions.service';
import { QuestionsController } from './questions.controller';

describe('QuestionsController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    // jest.setTimeout(15_000);

    app = await Test.createTestingModule({
      providers: [QuestionsService, AppFirebaseConfigurationService], //todo mock this
      controllers: [QuestionsController],
    }).compile();
  });

  describe('/api/questions/:page/:pageSize (GET)', () => {
    it('/api/questions/0/3 (GET)', async (done) => {
      const results = await app
        .get<QuestionsController>(QuestionsController)
        .getQuestions(0, 3);

      expect(results?.length).toBe(3);
      done();
    }, 60_000);
  });
});
