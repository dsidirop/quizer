import { Controller, Get, Param } from '@nestjs/common';
import { QuestionsService, RawQuestion } from '../services/questions.service';

@Controller('/questions')
export class QuestionsController {

  public constructor(private readonly appService: QuestionsService) { }

  @Get(':page/:pageSize')
  public async getQuestions(
    @Param('page') page: number,
    @Param('pageSize') pageSize: number
  ): Promise<RawQuestion[]> {
    return await this.appService.getQuestionsAsync(page, pageSize);
  }

}
