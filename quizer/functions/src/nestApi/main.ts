import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3_000);
}

bootstrap()
  .then(() => console.info('** [INFO] [Bootstrap] Complete!'))
  .catch(err => console.error('** [ERROR] [Bootstrap] Failed!', err));
