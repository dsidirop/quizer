// import { Inject } from '@angular/core';
// import { Question } from '@app-shared-contracts/models';
// import { RawQuestion } from '@app-shared-contracts/models/raw/raw-question.model';
// import { IQuestionsService } from '@app-shared-contracts/services/firestore/iquestions.service';
// import { IMapperService, IMapperServiceToken } from '@app-shared-contracts/services/mapper/imapper.service';
import { Injectable } from '@angular/core';
import * as admin from 'firebase-admin';
import { from as ixFrom } from 'ix/iterable';
import { filter as ixFilter, map as ixMap } from 'ix/iterable/operators';
import { AppFirebaseConfigurationService } from './app-firebase-configuration.service';

export interface RawQuestion {
  id: string;
  description: string;
  timeToAnswer?: number;
  correctAnswer: number;
  possibleAnswers: string[];
}

@Injectable()
export class QuestionsService {

  private readonly firestoreApp: admin.app.App;

  //public constructor(private readonly store: AngularFirestore) { }
  public constructor(readonly appFirebaseConfigurationService: AppFirebaseConfigurationService) {
    this.firestoreApp = appFirebaseConfigurationService.ensureConfigured();
  }

  public getQuestionsAsync = async (page: number, pageSize: number): Promise<RawQuestion[]> => { //0
    const querySnapshot = await this.firestoreApp.firestore() //1
      .collection('questions')
      .limit((page + 1) * pageSize)
      .get();

    const offset = page * pageSize;
    const maxIndex = offset + pageSize;
    const results = ixFrom(querySnapshot.docs) //transducer
      .pipe(
        ixFilter((_, i) => i >= offset && i <= maxIndex),
        ixMap(
          x => ({
            ...(x.data() as RawQuestion), //2
            id: x.id //questionid
          })
        )
      );

    return Array.from(results);

    //0 inspired by https://youtu.be/Eh9LLq3wApc?t=74
    //
    //1 todo   experiment with
    //
    //   .limit(count)
    //   .orderBy('timestamp', 'asc') //for some weird reason this causes the results list to be empty   but why?
    //   .startAfter(this.last)
    //
    //2 todo   this.mapper.converter2<RawQuestion, Question>({
    //            ...(x.data() as RawQuestion),
    //            id: x.id //questionid
    //         } as RawQuestion)
  }
}
