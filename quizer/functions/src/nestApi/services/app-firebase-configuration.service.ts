import { Injectable } from '@angular/core';
import * as admin from 'firebase-admin';
import { environment } from '../../environments/environment';

@Injectable()
export class AppFirebaseConfigurationService {

  public ensureConfigured(): admin.app.App { //temphack trick to make tests work in cicd in github
    console.debug(`** [INFO] [Nest] Checking if the Firestore needs to be configured (admin.apps.length=${admin.apps.length})`);
    if ((admin.apps.length ?? 1) > 0) {
      console.debug('** [INFO] [Nest] Firestore App already initialized -> returning pre-existing app ...');
      return admin
        .apps
        .filter(x => x.options.projectId === environment.firebaseServiceSpecs.project_id)[0];
    }

    console.debug('** [INFO] [Nest] Configuring Firestore ...');
    return admin.initializeApp({ //0
      projectId: environment.firebaseServiceSpecs.project_id, //vital
      databaseURL: environment.databaseURL,

      credential: admin.credential.cert(
        {
          projectId: environment.firebaseServiceSpecs.project_id,
          privateKey: environment.firebaseServiceSpecs.private_key.replace(/\\n/g, '\n'),
          clientEmail: environment.firebaseServiceSpecs.client_email
        } as admin.ServiceAccount
      )
    });

    //0 https://www.youtube.com/watch?v=Eh9LLq3wApc
  }

}
