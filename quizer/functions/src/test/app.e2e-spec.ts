import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../nestApi/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    jest.setTimeout(15_000);
  });

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/questions/:page/:pageSize (GET)', (done) => {
    return request(app.getHttpServer())
      .get('/questions/0/3')
      .end(function (err, res) {
        expect(err).toBe(null);
        expect(res?.status).toBe(200);
        expect(res?.body?.length).toBe(3);
        done();
      });
  }, 60_000);

});
