import * as appFirestoreServiceSpecs from './firestore-service-account.info.sensitive.json';
import { IAppFirestoreServiceSpecs } from './iapp-firestore-service-specs.model';

export const environment = {
  production: false,

  databaseURL: "https://quizer-d6b60.firebaseio.com/",
  firebaseServiceSpecs: appFirestoreServiceSpecs as unknown as IAppFirestoreServiceSpecs
};
