export interface IAppFirestoreServiceSpecs {
  type: string; //                          service_account
  auth_uri: string; //                      https://accounts.google.com/o/oauth2/auth,
  token_uri: string; //                     https://oauth2.googleapis.com/token,
  auth_provider_x509_cert_url: string; //   https://www.googleapis.com/oauth2/v1/certs

  client_id: string; //                     11...25
  project_id: string; //                    quizer-d6b60 or qu1z3r
  private_key: string; //                   -----BEGIN PRIVATE KEY-----\nMII...v75\n-----END PRIVATE KEY-----\n
  client_email: string; //                  firebase-adminsdk-pgn9d@quizer-d6b60.iam.gserviceaccount.com
  private_key_id: string; //                277...bf2
  client_x509_cert_url: string; //          https://www.googleapis.com/...gserviceaccount.com
}
