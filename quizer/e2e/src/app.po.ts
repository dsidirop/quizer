import { browser, by, element } from 'protractor';

export class AppPage {
  public async navigateToAsync(): Promise<unknown> {
    return await browser.get(browser.baseUrl) as Promise<unknown>;
  }

  public async checkIfTitleElementExistsAsync(): Promise<boolean> {
    return await element(by.css('app-root sign-in h1')).isPresent();
  }
}
