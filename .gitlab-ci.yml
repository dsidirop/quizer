# for gitlab yml supported features have a look at https://docs.gitlab.com/ee/ci/yaml/
# for proofs of concept cicd files for gitlab have a look at https://docs.gitlab.com/ee/ci/examples/
# for a full fledged gitlab cicd guide have a look at https://javascript-conference.com/blog/build-test-deployment-angular-gitlab-ci/
#
#   tags: is used to select specific runners from the list of all runners that are allowed to run this project
#

variables:
  GIT_DEPTH: '0' # Tells git to fetch all the branches of the project, required by the analysis task
  SONAR_USER_HOME: '${CI_PROJECT_DIR}/.sonar' # Defines the location of the analysis task cache

default:
  image: node:12.13.0-alpine
  before_script:
    - export BUILD_UNIX_TIMESTAMP=$(date +%s) # unix timestamp
    - echo -e "** PWD=$PWD"
    - echo -e "** SHELL=$SHELL"
    - echo -e "** CI_JOB_ID=$CI_JOB_ID"
    - echo -e "** CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME"
    - echo -e "** CI_COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA"
    - echo -e "** BUILD_UNIX_TIMESTAMP=$BUILD_UNIX_TIMESTAMP"
    - sed -i "s^#{CI_JOB_ID}^$CI_JOB_ID^g"                          "quizer/build-info.json" # 378
    - sed -i "s^#{CI_COMMIT_REF_NAME}^$CI_COMMIT_REF_NAME^g"        "quizer/build-info.json" # master
    - sed -i "s^#{CI_COMMIT_SHORT_SHA}^$CI_COMMIT_SHORT_SHA^g"      "quizer/build-info.json" # dd648b2e
    - sed -i "s^#{BUILD_UNIX_TIMESTAMP}^$BUILD_UNIX_TIMESTAMP^g"    "quizer/build-info.json" # 1591945553
    - "if ! (echo \"$CI_COMMIT_REF_NAME\" | grep -Eqi \"(^|/)master[^/]*$\"); then (echo \"**** Emitting FUNCTIONS_SERVICE_TOKEN__DEV__B64  into firestore-service-account.info.sensitive.json\"; echo $FUNCTIONS_SERVICE_TOKEN__DEV__B64   | base64 -d > quizer/functions/src/environments/firestore-service-account.info.sensitive.json); fi"
    - "if   (echo \"$CI_COMMIT_REF_NAME\" | grep -Eqi \"(^|/)master[^/]*$\"); then (echo \"**** Emitting FUNCTIONS_SERVICE_TOKEN__PROD__B64 into firestore-service-account.info.sensitive.json\"; echo $FUNCTIONS_SERVICE_TOKEN__PROD__B64  | base64 -d > quizer/functions/src/environments/firestore-service-account.info.sensitive.json); fi"
    # - npm i -g firebase-tools # <- dont   this would break sonarcube

stages:
  - test
  - deploy

# stage: testing

test-webui-unit-tests:
  stage: test
  image: trion/ng-cli-karma:9.1.7
  cache:
    key: '${CI_JOB_NAME}'
    paths:
      - .sonar/cache
  allow_failure: false
  script:
    - cd quizer
    - npm ci
    - npm i -g firebase-tools
    - npm i -g sonarqube-scanner
    - npm run test:ci:unit #order
    - npm run lint:ci:sonar -- -Dsonar.login=${SONAR_TOKEN} #order
  coverage: '/Lines \W+: (\d+\.\d+)%.*/'
  only:
    refs:
      - merge_requests
      - master
      - develop
    changes:
      - .gitlab-ci.yml
      - quizer/**/*
  except:
    changes:
      - quizer/functions/**/*
  artifacts:
    paths:
      - coverage/
  tags:
    - docker

test-webui-e2e:
  stage: test
  image: trion/ng-cli-e2e:9.1.7
  allow_failure: false
  script:
    - cd quizer
    - npm ci
    - npm i -g firebase-tools
    - npm run test:ci:e2e
  only:
    refs:
      - merge_requests
      - master
      - develop
    changes:
      - .gitlab-ci.yml
      - quizer/**/*
  except:
    changes:
      - quizer/functions/**/*
  tags:
    - docker

test-webui-lint:
  stage: test
  image: trion/ng-cli:9.1.7
  script:
    - cd quizer
    - npm ci
    - npm i -g firebase-tools
    - npm run lint
  only:
    changes:
      - .gitlab-ci.yml
      - quizer/**/*
  except:
    changes:
      - quizer/functions/**/*
  tags:
    - docker

test-functions-unit-tests:
  stage: test
  image: boiyaa/google-cloud-sdk-nodejs:latest
  allow_failure: false
  script:
    - cd quizer
    - npm ci
    - cd functions
    - npm ci
    - gcloud
      auth
      activate-service-account
      --key-file src/environments/firestore-service-account.info.sensitive.json
    - npm run test:ci
  only:
    changes:
      - .gitlab-ci.yml
      - quizer/functions/**/*
  tags:
    - docker

# we need gcloud to perform the service auth but we also need npm   vanilla google/cloud-sdk
# doesnt have npm preinstalled   so we resorted to using this image instead
#
#                 https://github.com/boiyama/docker-google-cloud-sdk-nodejs
#
test-functions-e2e-tests:
  stage: test
  image: boiyaa/google-cloud-sdk-nodejs:latest
  allow_failure: false
  script:
    - cd quizer
    - npm ci
    - cd functions
    - npm ci
    # - gcloud
    #   functions
    #   deploy
    #   api
    #   --runtime nodejs8
    #   --entry-point handler
    #   --trigger-http
    - gcloud
      auth
      activate-service-account
      --key-file src/environments/firestore-service-account.info.sensitive.json
    - npm run test:e2e
  only:
    refs:
      - merge_requests
      - master
      - develop
    changes:
      - .gitlab-ci.yml
      - quizer/functions/**/*
  tags:
    - docker

# stage: deploy

deploy-firestore-configuration:
  stage: deploy
  script:
    - cd quizer
    - npm ci
    - cd functions
    - npm ci
    - npm i -g firebase-tools
    - firebase deploy --only firestore --token $SECRETS__FIREBASE_TOKEN
  only:
    refs:
      - master
    changes:
      - .gitlab-ci.yml
      - quizer/firestore.rules
      - quizer/firestore.indexes.json

deploy-functions:
  stage: deploy
  script:
    - cd quizer
    - npm ci
    - cd functions
    - npm ci
    - cd ..
    - npm i -g firebase-tools
    - firebase deploy --only functions --token $SECRETS__FIREBASE_TOKEN
  only:
    refs:
      - master
    changes:
      - .gitlab-ci.yml
      - quizer/functions/**/*

deploy-webui:
  stage: deploy
  script:
    - cd quizer
    - npm ci
    - npm i -g firebase-tools
    - npm run build:prod
    - firebase deploy --only hosting --token $SECRETS__FIREBASE_TOKEN
  only:
    refs:
      - master
    changes:
      - .gitlab-ci.yml
      - quizer/**/*
  except:
    changes:
      - quizer/functions/**/*
  environment:
    name: production
    url: https://quizer-d6b60.web.app/
